package cent.decorator.model;

public class StarDecorator extends Decorator {

    public StarDecorator(IText text) {
        super(text);
    }

    public String decorateText(String text) {
        return "**************************\n******" + text + "*********\n****************************";
    }
}
