package cent.decorator.model;

public class Text implements IText {

    private String text;

    public Text(String text) {
        this.text = text;
    }

    public String returnText() {
        return this.text;
    }
}
