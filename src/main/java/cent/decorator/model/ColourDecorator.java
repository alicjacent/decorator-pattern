package cent.decorator.model;

public class ColourDecorator extends Decorator {

    public ColourDecorator(IText text) {
        super(text);
    }

    public String decorateText(String text) {
        return "\033[31m" + text;
    }
}
