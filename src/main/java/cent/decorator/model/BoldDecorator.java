package cent.decorator.model;

public class BoldDecorator extends Decorator {

    public BoldDecorator(IText text) {
        super(text);
    }

    public String decorateText(String text) {
        return "\033[1m" + text;
    }
}
