package cent.decorator.model;

public abstract class Decorator implements IText {

    private IText text;

    public Decorator(IText text) {
        this.text = text;
    }

    public String returnText() {
        return decorateText(text.returnText());
    }

    public abstract String decorateText(String text);
}
