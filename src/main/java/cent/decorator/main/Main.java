package cent.decorator.main;

import cent.decorator.model.*;

public class Main {

    public static void main(String[] args) {
        IText myText = new Text("blablabla");
        myText = new BoldDecorator(new ColourDecorator(new StarDecorator(myText)));
        System.out.println(myText.returnText());
    }
}
